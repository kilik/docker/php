#!/usr/bin/env bash

echo "================================================================================"
echo "= PHP FPM DEV                                                                  ="
echo "================================================================================"

if [[ "${XDEBUG}" == "1" ]]; then
  echo "XDEBUG         : ON"
  echo "!!! WARNING - XDEBUG is enabled (it's a perf killer) !!!"
  echo "set XDEBUG=0 in your .env to disable it"
  phpenmod xdebug
else
  echo "XDEBUG         : OFF (set XDEBUG=1 in your .env to enable it)"
  phpdismod xdebug
fi

# launch non dev entry point
exec /run.sh "$@"
