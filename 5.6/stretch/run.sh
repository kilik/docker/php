#!/usr/bin/env bash
[ "$DEBUG" = "true" ] && set -x

FPM_UID_DEFAULT=$(id -u www-data)
PROJECT_DIR_ID=`stat -c '%u' $PROJECT_DIR`

echo "----------------------------------------------------------"
echo "www-data UID: ${FPM_UID_DEFAULT}"
echo "$PROJECT_DIR UID: ${PROJECT_DIR_ID}"
echo "----------------------------------------------------------"
# Here we check if GID and UID are already defined properly or not
# i.e Do we have a volume mounted and with a different uid/gid ?
if [[ "${PROJECT_DIR_ID}" != "${FPM_UID_DEFAULT}" ]]; then
    if [ "$PROJECT_DIR_ID" != "0" ] && [ "$PROJECT_DIR_ID" != "$(id -u www-data)" ]; then
      echo "Need to change UID and GID."
      usermod -u $PROJECT_DIR_ID www-data
      groupmod -g $PROJECT_DIR_ID www-data
      chown -R www-data:www-data $PROJECT_DIR
      echo "UID and GID changed to $PROJECT_DIR_ID and $PROJECT_DIR_ID."
    fi
else
    echo "UID and GUI are OK !"
fi
echo "----------------------------------------------------------"

echo "----------------------------------------------------------"
exec "$@"
