#!/usr/bin/env bash
[ "$DEBUG" = "true" ] && set -x

FPM_UID_DEFAULT=$(id -u www-data)
PROJECT_DIR_ID=`stat -c '%u' $PROJECT_DIR`
CURRENT_UID=$(id -u)

echo "================================================================================"
echo "= PHP FPM                                                                      ="
echo "================================================================================"
echo "PROJECT_DIR    : ${PROJECT_DIR}"
echo "PROJECT_DIR UID: ${PROJECT_DIR_ID}"
# Here we check if GID and UID are already defined properly or not
# i.e Do we have a volume mounted and with a different uid/gid ?
if [[ "${PROJECT_DIR_ID}" != "${FPM_UID_DEFAULT}" ]]; then
    if [ "$PROJECT_DIR_ID" != "0" ] && [ "${CURRENT_UID}" == "0" ]; then
      echo "UID + GID      : Need to change UID and GID."
      TMP_HOMEDIR='/tmp/home/www-data'
      HOMEDIR=$(getent passwd www-data | awk -F: '{print $6}')
      mkdir -p "${TMP_HOMEDIR}" && usermod -d "${TMP_HOMEDIR}" www-data
      usermod -u "$PROJECT_DIR_ID" www-data
      groupmod -g "$PROJECT_DIR_ID" www-data
      usermod -d "$HOMEDIR" www-data
      chown -R www-data:www-data /run/php/
      rm -rf "${TMP_HOMEDIR}"
      echo "UID + GID      : changed to $PROJECT_DIR_ID and $PROJECT_DIR_ID."
    fi
else
    echo "UID + GID      : already OK"
fi
echo "www-data UID   : `id -u www-data`"

NAMED_PIPED="/tmp/stdout"

rm -rf "$NAMED_PIPED" || true
mkfifo --mode 600 "${NAMED_PIPED}" || true
chown "$PROJECT_DIR_ID" "${NAMED_PIPED}"
echo  "Named pipe: ($NAMED_PIPED) creation OK"

(tail -q -f ${NAMED_PIPED} >> /proc/self/fd/2 || pkill php-fpm) &

MAIN_PROCESS_USER="${MAIN_PROCESS_USER:-www-data}"
echo "Main user      : ${MAIN_PROCESS_USER}"
echo "----------------------------------------------------------"

if [[ "${CURRENT_UID}" == "0" ]]; then
    chown "${MAIN_PROCESS_USER}" /proc/self/fd/{1,2}
    exec su-exec "${MAIN_PROCESS_USER}" /usr/bin/tini -- "$@"
else
    exec /usr/bin/tini -- "$@"
fi
