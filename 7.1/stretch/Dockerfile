FROM debian:stretch-slim

MAINTAINER Michel NAUD <mitch@kilik.fr>

ENV DEBIAN_FRONTEND=noninteractive

# php
RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get install -y --no-install-recommends \
        ca-certificates \
        iptables \
        curl \
        openssh-client \
        nano \
        wget \
        lsb-release \
        apt-transport-https \
    && wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg \
    && echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" >> /etc/apt/sources.list.d/php.list \
    && apt-get update \
    && apt-get install -y --no-install-recommends \
        php7.1-bz2 \
        php7.1-cli \
        php7.1-curl \
        php7.1-fpm \
        php7.1-gd \
        php7.1-imap \
        php7.1-intl \
        php7.1-json \
        php7.1-ldap \
        php7.1-mbstring \
        php7.1-mcrypt \
        php7.1-mysql \
        php7.1-opcache \
        php7.1-odbc \
        php7.1-pgsql \
        php7.1-tidy \
        php7.1-xsl \
        php7.1-zip \
        php7.1-apcu \
        php7.1-bcmath \
        php7.1-geoip \
        php7.1-imagick \
        php7.1-mongodb \
        php7.1-ssh2 \
        php7.1-memcache \
        php7.1-tideways \
        zip wget unzip \
        iputils-ping \
        netcat \
        cron \
        procps \
        moreutils \
        make \
    && apt-get clean \
    && rm -Rf /var/lib/apt/lists/* /usr/share/man/* /usr/share/doc/*

COPY php-cli.ini    /etc/php/7.1/cli/conf.d/30-custom-php.ini
COPY php-fpm.ini    /etc/php/7.1/fpm/conf.d/30-custom-php.ini
COPY www.conf       /etc/php/7.1/fpm/pool.d/

# For custom Configuration that comes from outside (via a docker compose mount)
RUN mkdir /etc/php/7.1/fpm/user-conf.d && \
    echo "; Default empty file" > /etc/php/7.1/fpm/user-conf.d/example.conf && \
    mkdir /var/log/php && \
    mkdir -p /run/php && \
    chown www-data:www-data /run/php && \
    mkdir -p /var/www/home && \
    chown www-data:www-data /var/www && \
    sed -i 's/\/var\/www/\/var\/www\/home/g' /etc/passwd

# nice shell defaults
COPY .bashrc /root/.bashrc
COPY .bashrc /var/www/home/.bashrc

# fix timezone Europe/Paris
RUN ln -fs /usr/share/zoneinfo/Europe/Paris /etc/localtime && dpkg-reconfigure -f noninteractive tzdata

ENV APP_ENV prod
ENV PROJECT_DIR /var/www/html

RUN \
    mkdir -p $PROJECT_DIR && chown -R www-data:www-data $PROJECT_DIR \
    && mkdir -p $PROJECT_DIR/var/cache $PROJECT_DIR/var/log \
    && chown -R www-data:www-data $PROJECT_DIR/var/cache $PROJECT_DIR/var/log \
    && chown www-data:www-data /var/log/php

# start service
COPY run.sh /run.sh
RUN  chmod +x /run.sh

EXPOSE 9000

WORKDIR /var/www/html

ENTRYPOINT ["/run.sh"]
CMD /usr/sbin/php-fpm7.1 -R
