FROM kilik/php:7.1-stretch

MAINTAINER Michel NAUD <mitch@kilik.fr>

ENV DEBIAN_FRONTEND=noninteractive

# php
RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get install -y --no-install-recommends \
        git \
        php7.1-xdebug \
    && apt-get clean \
    && rm -Rf /var/lib/apt/lists/* /usr/share/man/* /usr/share/doc/*

COPY php-cli.ini    /etc/php/7.1/cli/conf.d/30-custom-php.ini
COPY php-fpm.ini    /etc/php/7.1/fpm/conf.d/30-custom-php.ini

# Install Composer
ENV COMPOSER_ALLOW_SUPERUSER 1
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN composer --version

# We install xdebug but disabled (enabled in entrypoint if needed)
COPY xdebug.ini /etc/php/7.1/mods-available/xdebug.ini

ENV APP_ENV dev

# start service
COPY run-dev.sh /run-dev.sh
RUN  chmod +x /run-dev.sh

EXPOSE 9000

WORKDIR /var/www/html

ENTRYPOINT ["/run-dev.sh"]
CMD /usr/sbin/php-fpm7.1 -R
