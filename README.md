# php-fpm + php-cli docker service

## To work on this project

```shell
cd ~/docker
git clone git@gitlab.com:kilik/docker/php.git
cd php
```

Tags: [see wiki](https://gitlab.com/kilik/docker/php/-/wikis/home)

## Build images locally

- version: the image tag
- path: local patch for Dockerfile
- registry: where to push images
- pull: pull source image first
- push: push image on registry

```shell
./build.sh --path 5.6/stretch --registry registry.kilik.fr/php --version 5.6-stretch
./build.sh --path 7.4/buster --registry registry.kilik.fr/php --version 7.4-buster --dev --timestamp
```

## CI - Continuous integration

This project don't use CI anymore (problems with shared runners).

Maintained tags: 

* 8.3, 8.3-bullseye
* 8.3-dev, 8.3-bullseye-dev
* 8.2, 8.2-bullseye
* 8.2-dev, 8.2-bullseye-dev
* 8.1, 8.1-bullseye
* 8.1-dev, 8.1-bullseye-dev
* 8.0, 8.0-bullseye (*end of php support 2023-11-26*)
* 8.0-dev, 8.0-bullseye-dev (*end of php support 2023-11-26*)
* 7.4, 7.4-buster (*end of php support 2022-11-28*)
* 7.4-dev, 7.4-buster-dev (*end of php support 2022-11-28*)

For maintainers:
```shell
./build.sh --path 7.4/buster --registry kilik/php --version 7.4 --pull --dev --timestamp --push
./build.sh --path 7.4/buster --registry kilik/php --version 7.4-buster --pull --dev --timestamp --push
./build.sh --path 8.0/bullseye --registry kilik/php --version 8.0 --pull --dev --timestamp --push
./build.sh --path 8.0/bullseye --registry kilik/php --version 8.0-bullseye --pull --dev --timestamp --push
./build.sh --path 8.1/bullseye --registry kilik/php --version 8.1 --pull --dev --timestamp --push
./build.sh --path 8.1/bullseye --registry kilik/php --version 8.1-bullseye --pull --dev --timestamp --push
./build.sh --path 8.2/bullseye --registry kilik/php --version 8.2 --pull --dev --timestamp --push
./build.sh --path 8.2/bullseye --registry kilik/php --version 8.2-bullseye --pull --dev --timestamp --push
./build.sh --path 8.3/bullseye --registry kilik/php --version 8.3 --pull --dev --timestamp --push
./build.sh --path 8.3/bullseye --registry kilik/php --version 8.3-bullseye --pull --dev --timestamp --push
```

# Use this image in your projects

## Understand volumes in image

* /var/www/html/web is your website root
* /var/www/html is your project root

docker-compose.yml sample:

```yml
services:
  php:
    image: kilik/php:7.4-buster
```

## ENV vars

* APP_ENV: (test | dev | prod) to change your application environment.
* ENVIRONMENT: (prod | dev) to enable/disable xdebug, xhprof,... on the fly
